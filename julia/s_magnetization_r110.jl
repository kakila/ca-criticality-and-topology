#' # Macroscopic "magnetization" of rule 110
#'     author : Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#'
#'     date : 29.10.2022


include("CA1D.jl")
using .CA1D
using Plots
using Random
using Statistics: mean

#' ## Configuration
#' We define the size of the spatio-temporal region.
N  = 513;   # size of the configuration
nT = 10000;  # number of time steps
confs = zeros(Bool, (N, nT+1)) # configurations needed

#' We define a simple macroscopic observable.
#' The sum of the configuration. It is a sort of "magentization"
mag(c) = vec(mean(c, dims=1))


#' initial configuration
m0 = 0.573;
c0 = shuffle((1:N) .< round(m0 * N));
confs[:,1] .= c0

#' Evalaute for several time steps
rh = neighborhood(1);
r_campled(x) = r110(x, 0.26, false)
step_rule!(confs, rh, r_campled, bc_periodic, nT);
m = mag(confs)

#' Print the spatio-temporal result
pl = Plots.plot(CA1D.plot(confs),
                Plots.plot(permutedims(m), 1:nT+1, yaxis=:flip,
                xlabel="magentization", ylabel="step"),
                layout=(1,2))

#' ## Campling probability
#' We now evaluate the same rule, with the same initial condition, but using
#' different probability of setting a cell to clamp value.

np    = 150;
H     = range(-0.5, 0.2, length=np);
c_val = ceil.(H) .> 0;
p0    = abs.(H);

reps = 5
M = zeros(Float64, (np, reps));
AC1 = zeros(Float64, (np, reps));
m_p = zeros(Float64, np);
ac1 = zeros(Float64, np);
for i in 1:reps
   for (p, c, j) in zip(p0, c_val, 1:np)
        step_rule!(confs, rh, x -> r110(x, p, c), bc_periodic, nT)
        local m_ = mag(confs)
        m_p[j] = mean(m_);
        ac1[j] = CA1D.acf1(m_ .- m_p[j])
    end
    AC1[:, i] .= ac1
    M[:, i] .= m_p
end

#' Plot the different magnetizations
pl_m = Plots.plot(H, M, legend=false, xlabel="H", ylabel="magnetization")

#' Plot the AC 1
pl_ac = Plots.plot(H, AC1, legend=false, xlabel="H", ylabel="ac1(m)")
