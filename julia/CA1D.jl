"""
Functions to run 1D CA

author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
"""
module CA1D

export neighborhood, r110, bc_periodic
export step_rule, step_rule!

using Plots
using DSP: xcorr

"""
    bc_periodic(h, sz)

Periodic boundary conditons on array of size sz

# Examples
```julia-repl
julia> h = [-2, 1, 2];
julia> bc_periodic(h, 10)
3-element Vector{Int64}:
 8
 1
 2
julia> h = [[-2, 8] [1, 10] [2, 12]];
julia> bc_periodic(h, 10)
2×3 Matrix{Int64}:
 8   1  2
 8  10  2
```
"""
function bc_periodic(h, sz)
  h_ = mod.(h, sz)
  h_[h_ .== zero(first(h))] .= sz
  return h_
end

"""
    neighborhood(d)

1D relative neighborhood with distance `d`.

The 1D neighbourhood of a cell is the cell itself and the cells at a
Manhattan distance of `d`.

Returns a vector with the relative indexes of the neighborhood.

# Examples
```julia-repl
julia> neighborhood(1)
3-element Vector{Integer}:
 -1
  0
  1
julia> neighborhood([1, 3])
5-element Vector{Integer}:
 -3
 -1
  0
  1
  3
```
"""
function neighborhood(d)
  h = [-d; [zero(first(d))]; d];
  # sorted array, with potentially multiple distances
  h = sort(h);
end

struct Rule
  base::Matrix{Int}
  out::Vector{Bool}
end

(r::Rule)(x::AbstractMatrix) = r.out[permutedims(muladd(r.base, x, 1))]
(r::Rule)(x::AbstractVector) = r.out[first(muladd(r.base, x, 1))]

function (r::Rule)(x::AbstractMatrix, prob, clamp)
    c = r(x)
    # add noise -> set to clamp
    msk = rand(Float64, size(c)) .< prob;
    view(c, msk) .= clamp;
    return c
end

function (r::Rule)(x::AbstractVector, prob, clamp)
    c = r(x)
    # add noise -> set to clamp
    if rand(Float64) .< prob
        c = clamp
    end
    return c
end


"""
    step_rule(conf::AbstractVector, neigh::AbstractVector, rule::Rule,
         bc::Function, n::Integer=1)

Apply `n` times `rule` to `conf`.

# Examples
```julia-repl
julia> rh = neighborhood(1);
julia> c = Vector{Bool}([0, 0, 0, 1, 0, 0, 0]);
julia> step_rule(c, rh, r110, bc_periodic, 6)
7×7 Matrix{Bool}:
 0  0  0  1  1  0  0
 0  0  1  1  1  0  0
 0  1  1  0  1  0  1
 1  1  1  1  1  1  1
 0  0  0  0  0  0  1
 0  0  0  0  0  1  1
 0  0  0  0  1  1  1
```
"""
function step_rule(conf, neigh, rule, bc::Function, n::Integer=1)
    sz = length(conf)
    idx = bc(reshape(1:sz, (1, sz)) .+ neigh, sz)
    confs = zeros(typeof(first(conf)), (sz, n+1))
    confs[:, 1] = conf
    for t=2:(n+1)
        view(confs, :, t) .= rule(view(confs, idx, t-1))
    end
    return confs
end

function step_rule!(confs, neigh, rule, bc::Function, n::Integer=1)
    sz = length(confs[:,1])
    idx = bc(reshape(1:sz, (1, sz)) .+ neigh, sz)
    for t=2:(n+1)
        view(confs, :, t) .= rule(view(confs, idx, t-1))
    end
end

struct Clamping
    value
    prob
end

function step_rule(conf, neigh, rule, bc::Function, clamping::Clamping,
                   n::Integer=1)
    state_type = typeof(first(conf))
    sz = length(conf)
    idx = bc(reshape(1:sz, (1, sz)) .+ neigh, sz)
    confs = zeros(state_type, (sz, n+1))
    confs[:, 1] = conf
    to_clamp = rand(Float64, (sz, n)) .< clamping.prob
    for t=2:(n+1)
        view(confs, :, t) .= rule(view(confs, idx, t-1))
        view(confs, view(to_clamp, :, t-1), t) .= clamping.value
    end
    return confs
end

function step_rule!(confs, neigh, rule, bc::Function, clamping::Clamping,
                   n::Integer=1)
    sz = length(confs[:,1])
    idx = bc(reshape(1:sz, (1, sz)) .+ neigh, sz)
    to_clamp = rand(Float64, (sz, n)) .< clamping.prob
    for t=2:(n+1)
        view(confs, :, t) .= rule(view(confs, idx, t-1))
        view(confs, view(to_clamp, :, t-1), t) .= clamping.value
    end
end

"""
    r110(x)

Apply rule 110 to the configuration `x`.

# Examples
```julia-repl
julia> c = digits.(Bool, 0:7, base=2, pad=3);
julia> r = r110.(c);
julia> [x=> y for (x,y) in zip(c, r)]
8-element Vector{Pair{Vector{Bool}, Bool}}:
 [0, 0, 0] => 0
 [1, 0, 0] => 0
 [0, 1, 0] => 1
 [1, 1, 0] => 1
 [0, 0, 1] => 1
 [1, 0, 1] => 1
 [0, 1, 1] => 1
 [1, 1, 1] => 0
julia> c = Vector{Bool}([0, 0, 0, 1, 0, 0, 0]);
julia> h = Vector{Integer}([-1, 0, 1]);
julia> confs = c[reshape(2:6, (1, 5)) .+ h]
3×5 Matrix{Bool}:
 0  0  0  1  0
 0  0  1  0  0
 0  1  0  0  0
julia> r110(confs)
1×5 Matrix{Bool}:
 0  1  1  0  0
```
"""
r110 = Rule(2 .^ permutedims(2:-1:0), digits(Bool, 110, base=2, pad=8))


"""
    plot(conf::AbstractArray)

Plot configuration using [`Plots.heatmap`](@ref).
"""
function plot(conf::Matrix{Bool}; kwargs...)
    h = heatmap(transpose(conf), yflip=true, c=cgrad([:white, :black]),
                legend = :none, axis=false, ticks=false,
                aspect_ratio=:equal)
    Plots.plot(h; kwargs...)
end


acf1(x) = xcorr(x, x)[length(x)+1]



function reduce(conf::AbstractMatrix, center, agg::Function)
    return @views [agg(conf[1:center-1,:]);
            permutedims(conf[center,:]);
            agg(conf[center+1:end,:])]
end

function reduce(conf::AbstractVector, center, agg::Function)
    return @views [agg(conf[1:center-1]);
                   conf[center];
                   agg(conf[center+1:end])]
end

##############
struct Neighbor{T<:Integer}
    loc::Vector{T}
    center::T
end

function Neighbor(d)
    rh = neighborhood(d)
    center = findfirst(rh .== zero(first(rh)))
    Neighbor(rh, center)
end



end
