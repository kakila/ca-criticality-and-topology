#' # Macroscopic "magnetization" of n-ary rule 110 for different topologies
#'     author : Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#'
#'     date : 29.10.2022


include("CA1D.jl")
using .CA1D
using Plots
using Random
using Statistics: mean

#' ## Configuration
#' We define the size of the spatio-temporal region.
N  = 499;   # size of the configuration
nT = 1500;  # number of time steps
confs = zeros(Bool, (N, nT+1)); # configurations needed

#' We define a simple macroscopic observable.
#' The sum of the configuration. It is a sort of "magentization"
mag(c) = vec(mean(c, dims=1))

#' initial configuration
m0 = 0.573;
c0 = shuffle((1:N) .< round(m0 * N));
confs[:,1] .= c0;

#' ## Campling probability
#' We now evaluate rules with different topologies, with the same initial
#' condition, but using different probability of setting a cell to clamp value.

#' topologies
d = [1 5 10 20];
nd = length(d);

#' clamping probabilities
np    = 150;
H     = range(-0.5, 0.2, length=np);
c_val = ceil.(H) .> 0;
p0    = abs.(H);

#' run over all combinations
@show Threads.nthreads()

reps = 5
M = zeros(Float64, (np, nd, reps));
AC1 = zeros(Float64, (np, nd, reps));
Threads.@threads for t in 1:nd
    local rh = neighborhood(d[1]:d[t]);
    dm = maximum(rh)
    local C = copy(confs)
    local m_p = zeros(Float64, np);
    local ac1 = zeros(Float64, np);
    for i in 1:reps
        for (p, c, j) in zip(p0, c_val, 1:np)
            local rc(x) = r110(x, p, c);
            local r(x) = (rc ∘ CA1D.reduce)(x, dm+1, z -> mean(z, dims=1) .>= 0.5);
            step_rule!(C, rh, r, bc_periodic, nT)
            local m_ = mag(C)
            m_p[j] = mean(m_);
            ac1[j] = CA1D.acf1(m_ .- m_p[j])
        end
        AC1[:, t, i] .= ac1
        M[:, t, i] .= m_p
    end
end

#' Plot the different magnetizations
_pl = (Plots.plot(H, M[:,i,:], legend=false,
                  xlabel="H", ylabel="magnetization", title="d: $(d[i])")
       for i in 1:nd)
pl_m = Plots.plot(_pl..., layout=nd)

#' Plot the AC 1
_pl = (Plots.plot(H, AC1[:,i,:], legend=false,
                  xlabel="H", ylabel="ac1(m)", title="d: $(d[i])")
       for i in 1:nd)
pl_ac = Plots.plot(_pl..., layout=nd)
