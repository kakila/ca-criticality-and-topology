#' # Rule 110
#'    author : Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#'
#'    date : 29.10.2022

include("CA1D.jl")
using .CA1D
using Plots

#' ## Configuration
#'  We define the size of the spatio-temporal region.
N  = 480;  # size of the configuration
nT = 640;  # number of time steps

#' We define the neighborhood where the rule will be applied.
#' It is parametrized by the distance to the nearest neighbor.
d  = 1;
rh = neighborhood(d);

#' Initial configuration
c0 = zeros(Bool, N);
c0[end÷2] = 1;

#' Evalaute for several time steps
C = step_rule(c0, rh, r110, bc_periodic, nT);

#' Print the spatio-temporal result
CA1D.plot(C)

#' ## Different neighborhoods
#' We now evaluate the same rule, with the same initial condition, but using
#' different topologies.
d = 1:6;
nd = length(d)
C_d = Matrix{Bool}[];
for i=1:nd
    local rh = neighborhood(d[i]);
    push!(C_d, step_rule(c0, rh, r110, bc_periodic, nT));
end

#' Plot the different evolutions
pl = Plots.plot((CA1D.plot(c; title="d: $_d") for (_d, c) in zip(d, C_d))..., layout=6)

#' If the initial configuration is random, the topology effect is not so pronounced.
#'  A random initial configuration doesn't have a natural distance.
c0_rnd = rand(Bool, N);
C_d = Matrix{Bool}[];
for i=1:nd
    local rh = neighborhood(d[i]);
    push!(C_d, step_rule(c0_rnd, rh, r110, bc_periodic, nT));
end

#' Plot the different evolutions
pl_rnd = Plots.plot((CA1D.plot(c; title="d: $_d") for (_d, c) in zip(d, C_d))..., layout=6)

