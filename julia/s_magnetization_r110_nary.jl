#' # Macroscopic "magnetization" of n-ary rule 110
#'     author : Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#'
#'     date : 29.10.2022


include("CA1D.jl")
using .CA1D
using Plots
using Random
using Statistics: mean

#' ## Configuration
#' We define the size of the spatio-temporal region.
N  = 499;   # size of the configuration
nT = 1500;  # number of time steps
confs = zeros(Bool, (N, nT+1)); # configurations needed

#' We define a simple macroscopic observable.
#' The sum of the configuration. It is a sort of "magentization"
mag(c) = vec(mean(c, dims=1))

#' initial configuration
m0 = 0.573;
c0 = shuffle((1:N) .< round(m0 * N));

#' n-ary neighbourhood
d  = 1:10;
rh = neighborhood(d);

#' n-ary clamped rule neighbourhood
r_campled(x) = r110(x, 0.20, false)
r110_nary(x) = (r_campled ∘ CA1D.reduce)(x, 11, z -> sum(z, dims=1) .>= 5);

#' Evaluate for several time steps
confs[:,1] .= c0
step_rule!(confs, rh, r110_nary, bc_periodic, nT);

#' Compute the magentizationfor each time step
m = mag(confs);

#' Print the spatio-temporal result
pl = Plots.plot(CA1D.plot(confs),
                Plots.plot(permutedims(m), 1:nT+1, yaxis=:flip,
                xlabel="magentization", ylabel="step", legend=false),
                layout=(1,2))


#' ## Campling probability
#' We now evaluate the same rule, with the same initial condition, but using
#' different probability of setting a cell to clamp value.

np    = 150;
H     = range(-0.5, 0.2, length=np);
c_val = ceil.(H) .> 0;
p0    = abs.(H);

reps = 5
M = zeros(Float64, (np, reps));
AC1 = zeros(Float64, (np, reps));
m_p = zeros(Float64, np);
ac1 = zeros(Float64, np);
for i in 1:reps
    for (p, c, j) in zip(p0, c_val, 1:np)
        local rc(x) = r110(x, p, c);
        local r(x) = (rc ∘ CA1D.reduce)(x, 11, z -> sum(z, dims=1) .>= 5);
        step_rule!(confs, rh, r, bc_periodic, nT)
        local m_ = mag(confs)
        m_p[j] = mean(m_);
        ac1[j] = CA1D.acf1(m_ .- m_p[j])
    end
    AC1[:, i] .= ac1
    M[:, i] .= m_p
end

#' Plot the different magnetizations
pl_m = Plots.plot(H, M, legend=false, xlabel="H", ylabel="magnetization")

#' Plot the AC 1
pl_ac = Plots.plot(H, AC1, legend=false, xlabel="H", ylabel="ac1(m)")
