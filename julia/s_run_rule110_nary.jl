#' # Rule 110
#'    author : Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#'
#'    date : 29.10.2022

include("CA1D.jl")
using .CA1D
using Plots

#' ## Configuration
#'  We define the size of the spatio-temporal region.
N  = 480;  # size of the configuration
nT = 640;  # number of time steps
confs = zeros(Bool, (N, nT+1)); # configurations needed

#' We define the neighborhood where the rule will be applied.
#' It is parametrized by the distance to the nearest neighbor.
d  = 1:20;
rh = neighborhood(d);

#' Initial configuration
c0 = zeros(Bool, N);
c0[(end÷2).+rh.+1] .= true;
confs[:, 1] = c0;

#' Define n-ary rule from basic rule
dm = maximum(d)
r110_nary(x) = (r110 ∘ CA1D.reduce)(x, dm+1, z -> sum(z, dims=1) .>= dm/2);

#' Evaluate for several time steps
step_rule!(confs, rh, r110_nary, bc_periodic, nT);

#' Print the spatio-temporal result
CA1D.plot(confs, title="d: $d")

#' ## Different neighborhoods
#' We now evaluate the same rule, with the same initial condition, but using
#' different topologies.
d = [1 3 6 12 24 48];
nd = length(d);
confs[:, 1] = c0;
C_d = Matrix{Bool}[];
for i=1:nd
    local rh = neighborhood(d[1]:d[i]);
    local r(x) = (r110 ∘ CA1D.reduce)(x, d[i]+1, z -> sum(z, dims=1) .>= d[i]/2)
    step_rule!(confs, rh, r, bc_periodic, nT);
    push!(C_d, copy(confs));
end

#' Plot the different evolutions
pl = Plots.plot((CA1D.plot(c; title="d: $_d") for (_d, c) in zip(d, C_d))...,
                layout=nd)

#' If the initial configuration is random, the topology effect is still visible.
c0_rnd = rand(Bool, N);
confs[:,1] .= c0_rnd;
C_d = Matrix{Bool}[];
for i=1:nd
    local rh = neighborhood(d[1]:d[i]);
    local r(x) = (r110 ∘ CA1D.reduce)(x, d[i]+1, z -> sum(z, dims=1) .>= d[i]/2)
    step_rule!(confs, rh, r, bc_periodic, nT);
    push!(C_d, copy(confs));
end

#' Plot the different evolutions
pl_rnd = Plots.plot((CA1D.plot(c; title="d: $_d") for (_d, c) in zip(d, C_d))...,
                    layout=nd)
