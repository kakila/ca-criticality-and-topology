using Weave
include("CA1D.jl")

for s in filter(x->occursin(r"s_.*.jl", x), readdir())
    println("Weaving $s ...")
    weave(s, out_path="./html")
end
