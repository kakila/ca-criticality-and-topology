# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#

## Rule 110
#

## Configuration
# We define the size of the spatio-temporal region.
#
N  = 480;  # size of the configuration
nT = 640;  # number of time steps

##
# We define the neighborhood where the rule will be applied.
# It is parametrized by the distance to the nearest neighbor.
#
d  = 1;
rh = neighborhood(d);

## Initial configuration
#
c0      = zeros (1, N);
c0(240) = 1;

## Evalaute for several time steps
#
C = CA_step(c0, @rule110, rh, @bc_periodic, nsteps=nT, return_history=true);

## Print the spatio-temporal result
#
figure (1);
imagesc (1 - C);
axis equal

## Different neighborhoods
# We now evaluate the same rule, with the same initial condition, but using
# different different topologies
#
d = 1:6;
C_d = {};
for i = 1:length(d)
  rh     = neighborhood(d(i));
  C_d{i} = CA_step(c0, @rule110, rh, @bc_periodic, nsteps=nT, return_history=true);
endfor

## 
# Plot the different evolutions
figure (2);
for i =1:length(C_d)
  subplot(2, 3, i)
  imagesc (1 - C_d{i});
  axis equal
  title(sprintf("d = %d", d(i)))
endfor

## 
# If the initial configuration is random, the topology effect is not so pronounced.
# A random initial configuration doesn't have a natural distance.
c0_rnd = randi(2, [1, N]) - 1;
d = 1:6;
C_d = {};
for i = 1:length(d)
  rh     = neighborhood(d(i));
  C_d{i} = CA_step(c0_rnd, @rule110, rh, @bc_periodic, nsteps=nT, return_history=true);
endfor

## 
# Plot the different evolutions
figure (3);
for i =1:length(C_d)
  subplot(2, 3, i)
  imagesc (1 - C_d{i});
  axis equal
  title(sprintf("d = %d", d(i)))
endfor



