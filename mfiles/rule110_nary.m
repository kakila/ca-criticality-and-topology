# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{v} =} rule110_naray(@var{x})
## Apply rule 110 to the configuration @var{x} of any odd size.
##
## @end defun

function v = rule110_nary(x)
  #
  # to convert neighboorhood to index of the rule results
  persistent base = 2.^[2:-1:0].'
  # the rule result
  persistent retval = fliplr (arrayfun (@str2double, dec2bin (110, 8)));
  
  agg = @(y) double(mean(y, 2) > 0.5);
  
  nx_h = ceil(size(x, 2) / 2);
  x = [agg(x(:, 1:(nx_h-1))) x(:, nx_h) agg(x(:, (nx_h+1):end))];

  # converts rows to indexes into the rule result
  idx = x * base + 1;
  v = retval(idx);
endfunction

%!demo
%! c = arrayfun(@str2double, dec2bin(0:31,5));
%! r = rule110_nary (c).';
%! printf("%d %d %d %d %d => %d\n", [c r].');
%! # --------------------------------------
%! # Prints all possible results of the rule.

%!demo
%! c = [0, 0, 0, 1, 0, 0, 0];
%! h = [-2, -1, 0, 1, 2];
%! rule110_nary (c(4 + h))

