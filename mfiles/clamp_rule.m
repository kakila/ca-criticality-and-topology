# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{v} =} clamp_rule(@var{x}, @var{p}, @var{clamp}, @var{r})
## Apply rule @var{r} to the configuration @var{x} with campling.
##
## @end defun

function v = clamp_rule(x, p, clamp, rule)
  v = rule(x);
  # add noise -> set to clamp
  msk = rand (size (v) ) < p;
  v(msk) = clamp;
endfunction
