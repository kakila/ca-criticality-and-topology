# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{v} =} acf1(@var{x})
## Autocorrelation function at delay 1.
##
## Needs package signal
##
## @end defun

function v = acf1(x)
  nc = size(x, 2);  # number of columns
  for n = 1:nc
    v(n) = xcorr(x(:,n), 1)(1); 
  endfor
endfunction


