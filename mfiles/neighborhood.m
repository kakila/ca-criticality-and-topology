# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{h} =} neighborhood(@var{d})
## @defunx {@var{h} =} neighborhood(@var{d}, @var{flat}=true)
## 1D relative neighborhood with distance @var{d}.
##
## The 1D neighbourhood of a cell is the cell itself and the cells at a
## Manhattan distance of @var{d}.
##
## Returns @var{h} a vector with the relative indexes of the neighborhood.
## 
## For example for @code{@var{d} = 1}, we have @code{@var{h} = [-1, 0, 1]}.
##
## @end defun

function h = neighborhood(d, flat=true)
  h = [-d, zeros(length(d), 1), d];
  if flat
    # sorted array, with potentially multiple distances
    h = unique (h(:)).';
  endif
endfunction

%!demo
%! neighborhood ([1;2;3])
