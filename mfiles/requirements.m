# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#

printf ("Install required packages ...\n")

deps = {"control", "signal"};
for p = deps
  p_info = pkg ("describe", p{1});
  if isempty(p_info{1})
    printf ("Installing '%s'\n", p{1});
    pkg ("install", "-forge", p{1});
  else
    printf ("'%s' already installed\n", p{1});
  endif
endfor

printf ("Done!\n")
