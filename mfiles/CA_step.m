# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{c} =} CA_step(@var{c0}, @var{rule}, @var{h}, @bar{bc}, @var{nsteps})
## Apply the 1D rule several steps.
##
## @end defun

function c = CA_step(c0, rule, rel_neigh, bc, nsteps=1, return_history=false)
  sz  = length (c0);
  idx = bc((1:sz).' + rel_neigh, sz);
  
  if return_history
    c       = zeros (nsteps + 1, sz);
    c(1, :) = c0;
    for it = 1:nsteps
      c(it+1, :) = rule (c(it, :)(idx))(:);
    endfor
  else
    c = c0;
    for it = 1:nsteps
      c(1,:) = rule (c(idx));
    endfor
  endif

endfunction

%!demo

