# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#

pkg load signal

## Macroscopic "magnetization" of rule 110
#

## Configuration
# We define the size of the spatio-temporal region.
#
N  = 499;   # size of the configuration
nT = 1500;  # number of time steps

##
# We define a simple macroscopic observable.
# The sum of the configuration. It is a sort of "magentization"
mag = @(c) mean (c, 2);

##
# initial configuration
m0 = 0.573;
c0 = (1:N < round (m0 * N))(randperm(N)) * 1.0;

##
# Probability of setting a cell to clamp value
H     = linspace (-0.99, 0.99, 25);
np    = length (H);
clamp = ceil (H);
p0    = abs (H);


## Different neighborhoods
# We now evaluate the same rule, with the same initial condition, but using
# different different topologies
#
d   = [1:10];
nd  = length (d);
m_d = zeros(nT + 1, nd, np);
m_d(1, :, :) = mag(c0);
for i = 1:nd
  rh = neighborhood(d(1:i).');
  for j = 1:np
    c  = c0;
    for it = 1:nT
      c   = CA_step(c, @rule110_nary, rh, @bc_periodic);
      
      # add noise -> set to clamp
      msk = rand(1, N) < p0(j);
      c(msk) = clamp(j);

      # compute magnetization
      m_d(it + 1, i, j) = mag(c);
    endfor  # over iterations
    ac_1(i, j) = acf1(m_d(:, i, j) - mean(m_d(:, i, j)));
  endfor  # over noise levels
endfor  # over neighborhoods 

## 
# Plot the different evolutions
figure (1);
plot (m_d(:,:,round(np/2)), "linewidth", 0.2)
ylabel ("magnetization (noiseless)")
xlabel ("iteration")
legend (strsplit (num2str (d)))
axis tight

## Autocorrelation
#
figure(2)
plot (H, ac_1, "linewidth", 1.0)
ylabel ("AC(1)")
xlabel ("clamping field H")
legend (strsplit (num2str (d)))
axis tight


