
# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
#

pkg load signal

## Size scaling effects in the magenitzation of rule 110 with clamping
## Parameters

sizes = 2.^(6:11); # size of the CA configuration
nN = length(sizes);
realizations = 10; # number of noise realizations
rep = 5;

crit_exp = zeros(10, rep);

cont = 1;

# Probability of setting a cell to clamp value
H = [linspace(-0.6, -0.4, 5), ...
  linspace(-0.4, -0.2, 17)(2:end-1), ...
  linspace(-0.2, 0.1, 5)];
nH = length(H);
clamp = ceil(H); # clamping value
p0 = abs(H); # probability of clamping


# We define a simple macroscopic observable.
# The sum of the configuration. It is a sort of "magentization"
magnetization = @(c) mean(c, 2);

# Define an iteration function
function [y, o] = CA_iter(step_f, c0, nT, p_, cl_, obs)
  N = length(c0);
  o = zeros(nT, 1);
  c = c0;
  idx = rand(nT, N) < p_;
  for it = 1:nT
    c = step_f(c);
    # clamping noise
    c(idx(it, :)) = cl_;
    # compute observable
    o(it + 1) = obs(c);
  endfor  # over it, iterations
  y = acf1(o - mean(o));
endfunction

# Magnetization of the initial configuration
m0 = 0.573;

for d = 1:10
  for k = 1:rep
    clear pp

    rh = neighborhood(d); # relative neighborhood

    # CA step function
    _step = @(x) logical(CA_step(x, @rule110, rh, @bc_periodic));

    ## Loops
    ac1_mean = ac1_var = zeros(nN, nH);
    prog_ = 0;
    tot_ = nN * nH;
    wbar = waitbar(prog_ / tot_, sprintf("Progress %d/%d", prog_, tot_));

    for i = 1:nN
      N = sizes(i);

      # Same initial condition for all noise realizations
      c0 = zeros(1, N, "logical"); 
      c0(randperm(N, round(m0 * N))) = 1;
      
      # Number of times teps
      #nT = max(ceil(1.6 * N), 100);
      nT = max(ceil(2 * N), 100);

      ac1 = zeros(realizations, nH);
      for j = 1:nH
        id_ = tic;
        p_ = p0(j); # probability of clamping
        cl_ = clamp(j); # clamping value 
        for ir = 1:realizations
          ac1(ir, j) = CA_iter(_step, c0, nT, p_, cl_, magnetization);
        endfor
        prog_ += 1;
        waitbar(prog_ / tot_, wbar, sprintf("Progress %d/%d : %.2f s", prog_, tot_, toc(id_)));
      endfor  # over j, noise levels
      ac1_mean(i, :) = mean(ac1);
      ac1_var(i, :) = var(ac1);

    endfor # over i, configuration sizes
    close(wbar)


    # find maximum of AC1
    [ac1_max, max_col] = max(ac1_mean, [], 2);
    max_idx = sub2ind([nN, nH], (1:nN).', max_col);
    ac1_max_var = ac1_var(max_idx);

    pp = polyfit(log10(sizes), log10(ac1_max'), 1);
    pp(1)

    #figure(cont)
    #loglogerr(sizes, ac1_max', sqrt(ac1_max_var), "~o");
    #hold on;
    #h = loglog(sizes([1 end]), 10.^(pp(2)) * sizes([1 end]).^pp(1), "linewidth", 1);
    #hold off;
    #ylabel("<AC1>|max")
    #xlabel("N")
    #legend(h, sprintf("~ N\\^%.2f", pp(1)), "location", "northwest", "fontsize", 12);
    #axis tight
    #cont = cont + 1;

    crit_exp(d, k) = pp(1);

  endfor # rep
endfor # ds

## Plots
m_cr_exp = mean(crit_exp');
sd_cr_exp = std(crit_exp');

figure(100);
errorbar(m_cr_exp, sd_cr_exp, '-o')
xlabel("d")
ylabel("exponent")




