# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{h} =} bc_periodic(@var{h}, @var{sz})
## Apply 1D periodic boundary conditions.
##
## The configuration has size @var{sz}.
##
## @end defun

function h_ = bc_periodic(h, sz)
  h_ = mod (h, sz);
  h_(h_ == 0) = sz;
endfunction

%!demo
%! h = [-2 1 2];
%! bc_periodic(h, 10)

%!demo
%! h = [-2  1  2
%!       8 10 12];
%! bc_periodic(h, 10)
