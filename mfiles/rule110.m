# Copyright (C) 2022 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

## -*- texinfo -*-
## @defun {@var{v} =} rule110(@var{x})
## Apply rule 110 to the configuration @var{x}.
##
## @end defun

function v = rule110(x)
  #
 
  # to convert neighboorhood to index of the rule results
  persistent base = 2.^[2:-1:0].'
  # the rule result
  persistent retval = fliplr (arrayfun (@str2double, dec2bin (110, 8)));
  # converts rows to indexes into the rule result
  idx = x * base + 1;
  v = retval(idx);
endfunction

%!demo
%! c = arrayfun(@str2double, dec2bin(0:7,3));
%! r = rule110 (c).';
%! printf("%d %d %d => %d\n", [c r].');
%! # --------------------------------------
%! # Prints all possible results of the rule.

%!demo
%! c = [0, 0, 0, 1, 0, 0, 0];
%! h = [-1, 0, 1];
%! rule110 (c((2:6).' + h))

