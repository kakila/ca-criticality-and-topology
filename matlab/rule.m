


function v = rule(v, d)
% v is a binary vector
% d is the topological distance

	n = length(v);

	viejo = v;
	nuevo = zeros(1, n);

	for pos = 1:n
		vec_i = mod(pos - d, n);
		vec_d = mod(pos + d, n);

		if vec_i == 0
			vec_i = n;
		end

		if vec_d == 0
			vec_d = n;
		end

		nei = [vec_i pos vec_d];

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%%%%%%%%% apply the rule %%%%%%%%%%%%%%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		if sum(all(viejo(nei) == [0 0 0])) == 1
			nuevo(pos) = 0;
		elseif sum(all(viejo(nei) == [0 0 1])) == 1
			nuevo(pos) = 1;
		elseif sum(all(viejo(nei) == [0 1 0])) == 1
			nuevo(pos) = 1;
		elseif sum(all(viejo(nei) == [0 1 1])) == 1
			nuevo(pos) = 1;
		elseif sum(all(viejo(nei) == [1 0 0])) == 1
			nuevo(pos) = 0;
		elseif sum(all(viejo(nei) == [1 0 1])) == 1
			nuevo(pos) = 1;
		elseif sum(all(viejo(nei) == [1 1 0])) == 1
			nuevo(pos) = 1;
		elseif sum(all(viejo(nei) == [1 1 1])) == 1
			nuevo(pos) = 0;
		end
	end
	v = nuevo;
end



