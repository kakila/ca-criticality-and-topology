

clear all
close all


n = 100; %size of the CA
tmax = 1000; %number of iterations
d = 1; %topology

xhi = 1;
prob = 0.975;

ini = 2; %number of cells starting with 1



res = zeros(tmax, n); %matrix containing the whole simulation, each row is the config at time t
res(1, randi(n, 1, ini)) = 1; %initial state (with ini cells = 1)
%res(1, 15) = 1; %same initial condition for all

for t = 2:tmax
	res(t, :) = rule(res(t - 1, :), d); %apply the rule in ca(t - 1)

	%add noise
	if (round(rand(1), 2) >= prob)
		pos_i = randi(n, 1, xhi);
		res(t, pos_i) = ~res(t, pos_i);
	end
end

colormap([1 1 1; 0 0 0]);
image(res .* 255);






