

clear all
close all


n = 100; %size of the CA
tmax = 1000; %number of iterations
d = 1; %topology

ini = 2; %number of cells starting with 1



res = zeros(tmax, n); %matrix containing the whole simulation, each row is the config at time t
res(1, randi(n, 1, ini)) = 1; %initial state (with ini cells = 1)
%res(1, 15) = 1; %same initial condition for all

for t = 2:tmax
	res(t, :) = rule(res(t - 1, :), d); %apply the rule in ca(t - 1)
end

colormap([1 1 1; 0 0 0]);
image(res .* 255);





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function v = rule(v, d)
% v is a binary vector
% d is the topological distance

	n = length(v);

	viejo = v;
	nuevo = zeros(1, n);

	for pos = 1:n
		vec_i = mod(pos - d, n);
		vec_d = mod(pos + d, n);

		if vec_i == 0
			vec_i = n;
		end

		if vec_d == 0
			vec_d = n;
		end

		nei = [vec_i pos vec_d];

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		%%%%%%%%% apply the rule %%%%%%%%%%%%%%
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		if sum(all(viejo(nei) == [0 0 0])) == 1
			nuevo(pos) = 0;
		elseif sum(all(viejo(nei) == [0 0 1])) == 1
			nuevo(pos) = 1;
		elseif sum(all(viejo(nei) == [0 1 0])) == 1
			nuevo(pos) = 1;
		elseif sum(all(viejo(nei) == [0 1 1])) == 1
			nuevo(pos) = 1;
		elseif sum(all(viejo(nei) == [1 0 0])) == 1
			nuevo(pos) = 0;
		elseif sum(all(viejo(nei) == [1 0 1])) == 1
			nuevo(pos) = 1;
		elseif sum(all(viejo(nei) == [1 1 0])) == 1
			nuevo(pos) = 1;
		elseif sum(all(viejo(nei) == [1 1 1])) == 1
			nuevo(pos) = 0;
		end
	end
	v = nuevo;
end







